<?php



function generate_auth_token_form($form, &$form_state){

$form = array();
    
    $authToken = variable_get('tokens',NULL);

  $form['usuario_zoho'] = array(
    '#type' => 'textfield',
    '#title' => t('Username or Email ID'),
    '#size' => 50,
    '#maxlength' => 40,
    '#description' => t('Username or Email ID account zoho'),
    );


  $form['password_zoho'] =  array(
    '#type' => 'password', 
    '#title' => t('Password'), 
    '#maxlength' => 40, 
    '#size' => 50,

    );

   if($authToken != NULL){


    $form['token_zoho'] = array(
    '#type' => 'textfield',
    '#title' => $authToken[0],
    '#size' => 50,
    '#maxlength' => 40,
    '#default_value' =>$authToken[1],
    '#disabled'=>TRUE,
    );



   }
  
   $form['submit'] = array(
  '#type' => 'submit', 
  '#value' => t('Generate Auth Token'));  


    return $form;


}

function generate_auth_token_form_submit($form, &$form_state){

 $usuario_zoho = $form_state['values']['usuario_zoho'];
 $password_zoho = $form_state['values']['password_zoho'];
  
generate_auth_token($usuario_zoho,$password_zoho);




}

function generate_auth_token($usuario_zoho,$password_zoho){

$username = $usuario_zoho;
$password = $password_zoho;
$param = "SCOPE=ZohoCRM/crmapi&EMAIL_ID=".$username."&PASSWORD=".$password;
$ch = curl_init("https://accounts.zoho.com/apiauthtoken/nb/create");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
$result = curl_exec($ch);


$anArray = explode("\n",$result);// el resultado en array


$authToken = explode("=",$anArray['2']);
/*/ Array, 2 element
0  AUTHTOKEN
1  3f2b61a7ebb2e9c83708687a0fb9f49a
*/

variable_set('tokens',$authToken);


curl_close($ch);

}

